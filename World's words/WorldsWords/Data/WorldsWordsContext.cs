﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace WorldsWords.Models
{
    public class WorldsWordsContext : DbContext
    {
        public WorldsWordsContext (DbContextOptions<WorldsWordsContext> options)
            : base(options)
        {
        }

        public DbSet<WorldsWords.Models.WorldDict> WorldDict { get; set; }
    }
}
