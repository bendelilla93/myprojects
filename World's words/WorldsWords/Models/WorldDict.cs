﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace WorldsWords.Models
{
    public class WorldDict
    {
        public int ID { get; set; }
        public string English { get; set; }
        public string Hungarian { get; set; }
    }
}
