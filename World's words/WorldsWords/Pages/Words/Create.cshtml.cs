﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using WorldsWords.Models;

namespace WorldsWords.Pages.Words
{
    public class CreateModel : PageModel
    {
        private readonly WorldsWords.Models.WorldsWordsContext _context;

        public CreateModel(WorldsWords.Models.WorldsWordsContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public WorldDict WorldDict { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.WorldDict.Add(WorldDict);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}