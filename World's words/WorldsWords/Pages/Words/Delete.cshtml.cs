﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WorldsWords.Models;

namespace WorldsWords.Pages.Words
{
    public class DeleteModel : PageModel
    {
        private readonly WorldsWords.Models.WorldsWordsContext _context;

        public DeleteModel(WorldsWords.Models.WorldsWordsContext context)
        {
            _context = context;
        }

        [BindProperty]
        public WorldDict WorldDict { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            WorldDict = await _context.WorldDict.FirstOrDefaultAsync(m => m.ID == id);

            if (WorldDict == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            WorldDict = await _context.WorldDict.FindAsync(id);

            if (WorldDict != null)
            {
                _context.WorldDict.Remove(WorldDict);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
