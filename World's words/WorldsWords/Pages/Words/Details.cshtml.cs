﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WorldsWords.Models;

namespace WorldsWords.Pages.Words
{
    public class DetailsModel : PageModel
    {
        private readonly WorldsWords.Models.WorldsWordsContext _context;

        public DetailsModel(WorldsWords.Models.WorldsWordsContext context)
        {
            _context = context;
        }

        public WorldDict WorldDict { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            WorldDict = await _context.WorldDict.FirstOrDefaultAsync(m => m.ID == id);

            if (WorldDict == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
