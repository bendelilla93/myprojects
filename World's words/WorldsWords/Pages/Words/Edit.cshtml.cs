﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WorldsWords.Models;

namespace WorldsWords.Pages.Words
{
    public class EditModel : PageModel
    {
        private readonly WorldsWords.Models.WorldsWordsContext _context;

        public EditModel(WorldsWords.Models.WorldsWordsContext context)
        {
            _context = context;
        }

        [BindProperty]
        public WorldDict WorldDict { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            WorldDict = await _context.WorldDict.FirstOrDefaultAsync(m => m.ID == id);

            if (WorldDict == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(WorldDict).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WorldDictExists(WorldDict.ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool WorldDictExists(int id)
        {
            return _context.WorldDict.Any(e => e.ID == id);
        }
    }
}
