﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WorldsWords.Models;

namespace WorldsWords.Pages.Words
{
    public class IndexModel : PageModel
    {
        private readonly WorldsWords.Models.WorldsWordsContext _context;

        public IndexModel(WorldsWords.Models.WorldsWordsContext context)
        {
            _context = context;
        }

        public IList<WorldDict> WorldDict { get;set; }

        public async Task OnGetAsync()
        {
            WorldDict = await _context.WorldDict.ToListAsync();
        }
    }
}
