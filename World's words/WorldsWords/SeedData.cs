﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using WorldsWords.Models;

namespace WorldsWords
{
    public class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new WorldsWordsContext(
                serviceProvider.GetRequiredService<DbContextOptions<WorldsWordsContext>>()))
            {
                // Look for any words.
                if (context.WorldDict.Any())
                {
                    return;   // DB has been seeded
                }

                context.WorldDict.AddRange(
                    new WorldDict
                    {
                        English = "blue",
                        Hungarian = "kék",
                    },

                    new WorldDict
                    {
                        English = "yellow",
                        Hungarian = "sárga",
                    },
                    new WorldDict
                    {
                        English = "red",
                        Hungarian = "piros",
                    },
                    new WorldDict
                    {
                        English = "green",
                        Hungarian = "zöld",
                    }
                );
                context.SaveChanges();
            }
        }
    }
}
